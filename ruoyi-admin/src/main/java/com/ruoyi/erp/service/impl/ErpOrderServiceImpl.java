package com.ruoyi.erp.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ruoyi.common.utils.uuid.IdUtils;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.erp.domain.ErpBatchInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.erp.mapper.ErpOrderMapper;
import com.ruoyi.erp.domain.ErpOrder;
import com.ruoyi.erp.service.IErpOrderService;
import com.ruoyi.common.core.text.Convert;

/**
 * 订单Service业务层处理
 * 
 * @author 畅聚科技.Ltd
 * @date 2021-02-28
 */
@Service
public class ErpOrderServiceImpl implements IErpOrderService 
{
    @Autowired
    private ErpOrderMapper erpOrderMapper;

    /**
     * 查询订单
     * 
     * @param id 订单ID
     * @return 订单
     */
    @Override
    public ErpOrder selectErpOrderById(String id)
    {
        return erpOrderMapper.selectErpOrderById(id);
    }

    /**
     * 查询订单列表
     * 
     * @param erpOrder 订单
     * @return 订单
     */
    @Override
    public List<ErpOrder> selectErpOrderList(ErpOrder erpOrder)
    {
        return erpOrderMapper.selectErpOrderList(erpOrder);
    }

    /**
     * 新增订单
     * 
     * @param erpOrder 订单
     * @return 结果
     */
    @Override
    public int insertErpOrder(ErpOrder erpOrder)
    {
        // 查询数据库今天是第几个订单
        ErpOrder query = new ErpOrder();
        Map<String, Object> params = new HashMap<>();
        params.put("beginCreateTime", DateUtils.getDate());
        params.put("endCreateTime", DateUtils.getAddNumDate(24));
        query.setParams(params);
        List<ErpOrder> list = erpOrderMapper.selectErpOrderList(query);
        int num = 10000 + list.size() + 1;
        erpOrder.setOrderNo("M" + DateUtils.dateTime() + num);

        erpOrder.setId(IdUtils.fastSimpleUUID());
        erpOrder.setCreateTime(DateUtils.getNowDate());
        return erpOrderMapper.insertErpOrder(erpOrder);
    }

    /**
     * 修改订单
     * 
     * @param erpOrder 订单
     * @return 结果
     */
    @Override
    public int updateErpOrder(ErpOrder erpOrder)
    {
        erpOrder.setUpdateTime(DateUtils.getNowDate());
        return erpOrderMapper.updateErpOrder(erpOrder);
    }

    /**
     * 删除订单对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteErpOrderByIds(String ids)
    {
        return erpOrderMapper.deleteErpOrderByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除订单信息
     * 
     * @param id 订单ID
     * @return 结果
     */
    @Override
    public int deleteErpOrderById(String id)
    {
        return erpOrderMapper.deleteErpOrderById(id);
    }
}
