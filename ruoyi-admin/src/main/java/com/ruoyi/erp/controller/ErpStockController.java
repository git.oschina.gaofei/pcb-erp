package com.ruoyi.erp.controller;

import java.util.List;

import com.ruoyi.erp.domain.ErpModel;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.erp.domain.ErpStock;
import com.ruoyi.erp.service.IErpStockService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 产品库存Controller
 * 
 * @author 畅聚科技.Ltd
 * @date 2021-02-28
 */
@Controller
@RequestMapping("/erp/erpStock")
public class ErpStockController extends BaseController
{
    private String prefix = "erp/erpStock";

    @Autowired
    private IErpStockService erpStockService;

    @RequiresPermissions("erp:erpStock:view")
    @GetMapping()
    public String erpStock()
    {
        return prefix + "/erpStock";
    }

    /**
     * 查询产品库存列表
     */
    @RequiresPermissions("erp:erpStock:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(ErpStock erpStock)
    {
        startPage();
        List<ErpStock> list = erpStockService.selectErpStockList(erpStock);
        return getDataTable(list);
    }

    /**
     * 导出产品库存列表
     */
    @RequiresPermissions("erp:erpStock:export")
    @Log(title = "产品库存", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ErpStock erpStock)
    {
        List<ErpStock> list = erpStockService.selectErpStockList(erpStock);
        ExcelUtil<ErpStock> util = new ExcelUtil<ErpStock>(ErpStock.class);
        return util.exportExcel(list, "erpStock");
    }

    /**
     * 新增产品库存
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存产品库存
     */
    @RequiresPermissions("erp:erpStock:add")
    @Log(title = "产品库存", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ErpStock erpStock)
    {
        return toAjax(erpStockService.insertErpStock(erpStock));
    }

    /**
     * 修改产品库存
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        ErpStock erpStock = erpStockService.selectErpStockById(id);
        mmap.put("erpStock", erpStock);
        return prefix + "/edit";
    }

    /**
     * 修改保存产品库存
     */
    @RequiresPermissions("erp:erpStock:edit")
    @Log(title = "产品库存", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ErpStock erpStock)
    {
        return toAjax(erpStockService.updateErpStock(erpStock));
    }

    /**
     * 删除产品库存
     */
    @RequiresPermissions("erp:erpStock:remove")
    @Log(title = "产品库存", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(erpStockService.deleteErpStockByIds(ids));
    }

    /**
     * 查看详细
     */
    @GetMapping("/detail/{id}")
    public String detail(@PathVariable("id") String id, ModelMap mmap) {
        ErpStock erpStock = erpStockService.selectErpStockById(id);
        mmap.put("erpStock", erpStock);
        return prefix + "/detail";
    }
}
